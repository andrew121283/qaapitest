process.env.AWS_REGION = 'us-east-1';

const {
  STAGE: stage,
  ALIAS: alias,
  AWS_REGION: region,
} = process.env;

// Set serverless environment variables for util-config
process.env.SERVERLESS_STAGE = stage;
process.env.SERVERLESS_ALIAS = alias;

// eslint-disable-next-line no-console
console.log(`Environment: stage=${stage} alias=${alias} region=${region}\n`);

module.exports = {
  testPathIgnorePatterns: ['/node_modules/', 'dist'],
  verbose: true,
  automock: false,
  testEnvironment: 'node',
  rootDir: '../../',
  preset: 'ts-jest/presets/js-with-babel',
  moduleFileExtensions: ['ts', 'js'],
  moduleNameMapper: {
    '@src(.*)': '<rootDir>/src$1',
    '@test(.*)': '<rootDir>/test$1',
    '@insomnia(.*)': '<rootDir>/insomnia$1',
  },
  setupFilesAfterEnv: ['<rootDir>/test/qaapitests/jest.setup.js'],
  testMatch: ['**/*.api.test.ts']
};
