import { group, requests } from '../../../../insomnia';
import importGraphQL from '../../../../src/utils/importGraphQL';

type createQuery = { name: string, variables?: boolean };
const Query = ({ name, variables }: createQuery) => importGraphQL(__dirname, 'queries', name, variables);

const schemaGroup = group({
  sort: 4,
  name: 'Referrals',
  parent: 'fld_54554f2832d040e4b68de0cd51883695',
});

const queryGroup = group({
  sort: 0,
  name: 'Query',
  parent: schemaGroup._id,
});

export const queries = {
  canLinkReferralParent: Query({ name: 'canLinkReferralParent' }),
  completedReferrals: Query({ name: 'completedReferrals' }),
  pendingReferrals: Query({ name: 'pendingReferrals' }),
  getReferralScreen: Query({ name: 'getReferralScreen' }),
};

const queryRequests = requests({
  queries,
  parent: queryGroup._id,
  path: '{{ authenticated }}',
});


export const workspace = [
  schemaGroup,
  queryGroup,
  ...queryRequests,
];
