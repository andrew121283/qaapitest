/*  Filename: referrals.api.test.ts
    Test Suite(s):   ReferralScreen
    Tets Description: Check that ReferralScreen, PendingRequests, CompletedRequests  are returned
*/
import { client, environment } from '@test/environment';
import { queries } from './insomnia';


const userCognitoId = environment.prod
  ? 'us-east-1:77951e2f-a121-406a-8a00-71200f137390'
  : 'us-east-1:0eac8f0c-2fae-49ed-b3f9-912d4c055700'; // dev cognitoID 4505550101

const lambda = client('agraphql');

describe('Referrals', () => {

  describe('Query', () => {

    describe('cannotLinkReferralParent', () => {
      //Test for account that already has parent account
      test('returns a boolean indicating referral link eligibility for account that already has parent account', async () => {
        const { query } = queries.canLinkReferralParent;
        const response = await lambda.invoke({
          warmup: true,
          payload: { query },
          userCognitoId,
        });

        expect(response.statusCode).toBe(200);

        const {
          data: {
            canLinkReferralParent: { canLinkReferralParent: canLink },
          },
          errors,
        } = JSON.parse(response.body);

        expect(errors).toBeUndefined();
        expect(canLink).toBe(false);
      });
    });
    
    describe('canLinkReferralParent', () => {
      //Test for account that does not have parent account(needs to create each test)
      test.skip('returns a boolean indicating referral link eligibility for account that does not have parent account', async () => {
        const { query } = queries.canLinkReferralParent;
        const userCognitoId = environment.prod
  ? 'us-east-1:77951e2f-a121-406a-8a00-71200f137390'
  : 'us-east-1:31b1c1d9-9b40-4cea-b05b-d796795ec962'; // dev cognitoID 4505550109

        const response = await lambda.invoke({
          warmup: true,
          payload: { query },
          userCognitoId,
        });

        expect(response.statusCode).toBe(200);

        const {
          data: {
            canLinkReferralParent: { canLinkReferralParent: canLink },
          },
          errors,
        } = JSON.parse(response.body);

        expect(errors).toBeUndefined();
        expect(canLink).toBe(true);
      });
    });

    describe('referralScreen', () => {
    // Test - referralScreen 
      test('returns all data from Refer screen', async () => {
        const { query } = queries.getReferralScreen;
        const response = await lambda.invoke({
          warmup: true,
          payload: { query },
          userCognitoId,
        });
  
        expect(response.statusCode).toBe(200);
  
        const {
          data: {
            pendingReferrals,
            completedReferrals,
            referralProgram: {
              title, 
              description },
            referralShareMessages:{
              genericMessage,
              facebookPlaceholder,
              smsMessage,
              twitterMessage },
            referralLink,
          },
          errors,
        } = JSON.parse(response.body);
  
        /* ASSERTIONS */
  
        expect(errors).toBeUndefined(); // Are there any errors?
  
        // Referral Link
        expect(referralLink.code).toBeDefined();
        expect(referralLink.code).toMatch('AUTOT1'); // Is the correct ReferralCode generated
        expect(referralLink.url).toBeDefined();
        const link = environment.prod
          ? 'link.dosh.cash'
          : 'staging-dosh.app.link';
        expect(referralLink.url).toMatch(new RegExp(`${link}/${'AUTOT1'}`)); // Is the correct Referral URL generated?
  
        //Pending Referrals
        expect(pendingReferrals.totalItems).toBeDefined();
        expect(pendingReferrals.totalItems).toBe(1); // Is there correct number of the Pending Referrals
  
        //Completed Referrals
        expect(completedReferrals.totalItems).toBeDefined();
        expect(completedReferrals.totalItems).toBe(1); // Is there correct number of the Completed Referrals
  
        // Referral Program
        expect(title).toMatch('Invite Friends, Get $5');
        expect(description.plainText).toMatch('Get $5 for each friend who signs up with your link by connecting and verifying their credit or debit card.');
        expect(description.htmlText).toMatch('Get $5 for each friend who signs up with your link by <a href=\"https://doshsupport.zendesk.com/hc/en-us/articles/360010908854-How-do-I-connect-a-card-or-add-additional-cards-\">connecting</a> and <a href=\"https://doshsupport.zendesk.com/hc/en-us/articles/360011875934-What-is-a-verified-account-\">verifying</a> their credit or debit card.');
        expect(description.markdownText).toMatch('Get $5 for each friend who signs up with your link by [connecting](https://doshsupport.zendesk.com/hc/en-us/articles/360010908854-How-do-I-connect-a-card-or-add-additional-cards-) and [verifying](https://doshsupport.zendesk.com/hc/en-us/articles/360011875934-What-is-a-verified-account-) their credit or debit card.');
  
        // Referral Share Messages
        expect(errors).toBeUndefined();
        expect(genericMessage).toMatch("Hey! Check out the Dosh app. It pays you automatically when you shop and dine out. Download the app, link a card, and start getting cash back.");
        expect(facebookPlaceholder).toMatch("Tell your friends why you love Dosh!");
        expect(smsMessage).toMatch("Hey! Check out the Dosh app. It pays you automatically when you shop and dine out. Download the app, link a card, and start getting cash back.");
        expect(twitterMessage).toMatch( "Hey! Check out the Dosh app. It pays you automatically when you shop and dine out. Download the app, link a card, and start getting cash back.");
      });
    });
    
    describe('completedReferrals', () => {
      //Completed Referrals Screen Test
     test('returns a list of completed referrals', async () => {
       const { query } = queries.completedReferrals;
       const response = await lambda.invoke({
         warmup: true,
         payload: { query },
         userCognitoId,
       });

       expect(response.statusCode).toBe(200);

       const {
         data: {
           completedReferrals: { totalItems, items },
           referralStats: { referralsLifetimeCashBack }
         },
         errors,
       } = JSON.parse(response.body);

       expect(errors).toBeUndefined();

       //Completed Referrals Screen Results
       expect(totalItems).toBe(1);
       items.results.forEach((result) => {
        expect(result.avatar).toBeNull;
        expect(result.initials).toStrictEqual("AT");
        expect(result.title).toStrictEqual("AutoReferral Test");
        expect(result.details).toMatch('$5 bonus paid on 09/25/2019');
        expect(items.pagination.cursor).toBeNull;
        expect(items.pagination.hasNextPage).toBeFalsy;
        expect(referralsLifetimeCashBack).toBeNull;
      });
     });
   });

   describe('completedReferralsBlankscreen', () => {
    //Completed Referrals Screen Test for blank screen
   test('returns a blank completed referrals screen', async () => {
     const { query } = queries.completedReferrals;
     const userCognitoId = environment.prod
  ? 'us-east-1:77951e2f-a121-406a-8a00-71200f137390'
  : 'us-east-1:31b1c1d9-9b40-4cea-b05b-d796795ec962'; // dev cognitoID 4505550109
     const response = await lambda.invoke({
       warmup: true,
       payload: { query },
       userCognitoId,
     });

     expect(response.statusCode).toBe(200);

     const {
       data: {
         completedReferrals: { totalItems, items },
       },
       errors,
     } = JSON.parse(response.body);

     expect(errors).toBeUndefined();
     //Completed Referrals Screen for the new user
     expect(totalItems).toStrictEqual(0);
     expect(items.results.length).toBeLessThanOrEqual(totalItems);
     expect(items.results).toBeNull;
     expect(items.results.length).toBeLessThanOrEqual(totalItems);
     expect(items.pagination.cursor).toBeNull;
     expect(items.pagination.hasNextPage).toStrictEqual(false);
   });
 });

   describe('pendingReferrals', () => {
     //Pending Referrals Screen Test
     test('returns a list of pending referrals', async () => {
       const { query } = queries.pendingReferrals;
       const response = await lambda.invoke({
         warmup: true,
         payload: { query },
         userCognitoId,
       });

       expect(response.statusCode).toBe(200);

       const {
         data: {
           pendingReferrals: { totalItems, items },
         },
         errors,
       } = JSON.parse(response.body);

       expect(errors).toBeUndefined();
       expect(totalItems).toBe(1);
       expect(items.results.length).toBeLessThanOrEqual(totalItems);
       //Pending Referrals Screen Results
       items.results.forEach((result) => {
         expect(result.avatar.url).toMatch("https://s3.amazonaws.com/doshdev-assets/images/users/avatar/e00ca7e2-ab6e-448b-8cfb-1a096e54a351.jpg")
         expect(result.avatar.scalingMode).toMatch("FIT")
         expect(result.initials).toStrictEqual("AT");
         expect(result.title).toStrictEqual("Auto Test");
         expect(result.details).toStrictEqual("Pending card link");
         expect(result.extendedDetails).toBe(null);
       });
       expect(items.pagination.cursor).toBeFalsy;
       expect(items.pagination.hasNextPage).toBeFalsy;
      
     });
   });
   
   describe('pendingReferralsBlankscreen', () => {
    //Pending Referrals Screen Test for blank screen
   test('returns a blank pending referrals screen', async () => {
     const { query } = queries.pendingReferrals;
     const userCognitoId = environment.prod
  ? 'us-east-1:77951e2f-a121-406a-8a00-71200f137390'
  : 'us-east-1:31b1c1d9-9b40-4cea-b05b-d796795ec962'; // dev cognitoID 4505550109
     const response = await lambda.invoke({
       warmup: true,
       payload: { query },
       userCognitoId,
     });

     expect(response.statusCode).toBe(200);

     const {
       data: {
         pendingReferrals: { totalItems, items },
       },
       errors,
     } = JSON.parse(response.body);

     expect(errors).toBeUndefined();
     //Pending Referrals Screen for the new user
     expect(totalItems).toStrictEqual(0);
     expect(items.results.length).toBeLessThanOrEqual(totalItems);
     expect(items.results).toBeNull;
     expect(items.results.length).toBeLessThanOrEqual(totalItems);
     expect(items.pagination.cursor).toBeNull;
     expect(items.pagination.hasNextPage).toStrictEqual(false);
   });
 });
  });
});
